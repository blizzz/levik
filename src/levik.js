function init() {
    const content = document.getElementById('contentWrapper');

    const text = document.createElement('h1');
    text.innerText = "Hallo Welt, dit is Levik!"

    content.appendChild(text);

    const taskWrapper = document.createElement('p');
    taskWrapper.classList.add('task')

    const operand1  = document.createElement('div');
    operand1.id = 'operand_1';
    operand1.classList.add('operand');
    taskWrapper.appendChild(operand1);

    const operator = document.createElement('div');
    operator.classList.add('operator');
    operator.innerText = '+';
    taskWrapper.appendChild(operator);

    const operand2 = document.createElement('div');
    operand2.id = 'operand_2';
    operand2.classList.add('operand');
    taskWrapper.appendChild(operand2);

    const solutionWrapper = document.createElement('p');
    solutionWrapper.appendChild(buildSolutionElement());
    taskWrapper.appendChild(solutionWrapper);

    const buttonWrapper = document.createElement('div');

    const testButton = document.createElement('button');
    testButton.id = 'testSolution';
    testButton.innerText = 'Ergebnis prüfen';
    testButton.title = 'Geht auch mit der Enter-Taste :)';
    testButton.onclick = testSolution;
    buttonWrapper.appendChild(testButton);

    const nextTaskButton = document.createElement('button');
    nextTaskButton.id = 'testSolution';
    nextTaskButton.innerText = 'Neue Aufgabe';
    nextTaskButton.title = 'Geht auch mit der n-Taste :)';
    nextTaskButton.onclick = newTask;
    buttonWrapper.appendChild(nextTaskButton);

    taskWrapper.appendChild(buttonWrapper);

    const resultWrapper = document.createElement('div');

    const successText = document.createElement('span')
    successText.innerText = 'Perfekt! :)';
    successText.classList.add('solution_good', 'solution_text')
    resultWrapper.appendChild(successText);

    const failureText = document.createElement('span')
    failureText.innerHTML = 'Leider falsch :(';
    failureText.classList.add('solution_bad', 'solution_text');
    resultWrapper.appendChild(failureText);

    taskWrapper.appendChild(resultWrapper);

    content.appendChild(taskWrapper);

    newTask();
}

function getFirstOperand() {
    return Math.floor(Math.random() * (1000000 - 10000) + 10000);
}

function getSecondOperand(maxValue) {
    return Math.floor(Math.random() * maxValue);
}

function buildSolutionElement() {
    const solutionWrapper = document.createElement('p');
    solutionWrapper.id = 'solutionWrapper';
    solutionWrapper.classList.add('digits');

    for(let i = 0; i < 6; i++) {
        const inputDigit = document.createElement('input');
        inputDigit.dataset.factor = Math.pow(10, i).toString();
        inputDigit.type = 'number';
        inputDigit.min = '0';
        inputDigit.max = '9';
        inputDigit.placeholder = '0';
        inputDigit.onkeyup = onDigitKeyUp;
        inputDigit.onfocus = inputDigit.select;
        if (i === 0) {
            inputDigit.id = 'rightmost_digit';
        }
        solutionWrapper.prepend(inputDigit)
    }

    return solutionWrapper;
}

function testSolution() {
    const solutionWrapper = document.getElementById('solutionWrapper');
    const digits = solutionWrapper.getElementsByTagName("input");
    let provided = 0;
    for (const digit of digits) {
        provided += parseInt(digit.dataset.factor, 10) * parseInt(digit.value, 10)
    }
    const isSolutionCorrect = provided.toString() === solutionWrapper.dataset.solution;

    const e = document.getElementsByClassName(isSolutionCorrect ? 'solution_good' : 'solution_bad');
    console.debug(e);
    e[0].style.display = 'block';
    setTimeout(function () {
        e[0].style.display = 'none';
    }, 1500);
}

function newTask() {
    const op1Value = getFirstOperand();
    const op2Value = getSecondOperand(1000000 - op1Value);

    const operand1 = document.getElementById('operand_1');
    operand1.innerText = op1Value.toString();

    const operand2 = document.getElementById('operand_2');
    operand2.innerText = op2Value.toString();

    const solution = document.getElementById('solutionWrapper');
    solution.dataset.solution = (op1Value + op2Value).toString()

    for (let digitInput of solution.children) {
        digitInput.value = '';
    }

    document.getElementById('rightmost_digit').focus();
}

function onDigitKeyUp(event) {
    if (event.keyCode === 13) {
        testSolution();
    } else if (event.keyCode >= 48 && event.keyCode <= 57 && this.previousElementSibling) {
        this.previousElementSibling.focus();
    } else if (event.key === 'ArrowLeft' && this.previousElementSibling) {
        this.previousElementSibling.focus();
    } else if (event.key === 'ArrowRight' && this.nextElementSibling) {
        this.nextElementSibling.focus();
    } else if (event.key === 'n') {
        newTask();
    } else {
        this.select();
    }
}

document.body.onload = init;
